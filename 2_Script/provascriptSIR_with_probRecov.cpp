#include <Rcpp.h>
using namespace Rcpp;
using namespace std;


  // Function to check comment element between Column of a datframe and a vector
  // The function compares the name stored in a column (index given column_number) of a dataframe (net)
  // and those in a vector (S_vec)
  // The function returns a vector with the cmmon names
  // [[Rcpp::export]]
  CharacterVector commonelements_DfVec(DataFrame net,  CharacterVector S_vec, int column_number){
    DataFrame filtered_net = net;
    CharacterVector is_in_S_vecf; // vector containing results of comparaison
    CharacterVector dest=filtered_net[column_number]; // selecting column of interest
    CharacterVector is_in_S_vec(S_vec.size() + dest.size()); //creating empty vector containing common elements
    
    //iterator to store return type
    CharacterVector::iterator it, end;
    
    end = set_intersection(
      S_vec.begin(), S_vec.end(),
      dest.begin(), dest.end(),
      is_in_S_vec.begin());// vector containing common elements and empty spaces 
    
    for(int i=0; i < is_in_S_vec.size(); ++i){//pushing only elements not empty values
      if(is_in_S_vec(i)!=""){
        is_in_S_vecf.push_back(is_in_S_vec(i));
      }
      
    }
    
    return(is_in_S_vecf);
  }




// [[Rcpp::export]]
CharacterVector commonelements_ListVec(List net,  CharacterVector S_vec){
  List filtered_net = net;
  CharacterVector is_in_S_vecf;// vector containing results of comparaison
  CharacterVector dest=filtered_net.names();// extracting the names of the list key
  CharacterVector is_in_S_vec(S_vec.size() + dest.size());//creating empty vector containing common elements
  
  //iterator to store return type
  CharacterVector::iterator it, end;
  
  end = set_intersection(
    S_vec.begin(), S_vec.end(),
    dest.begin(), dest.end(),
    is_in_S_vec.begin());// vector containing common elements and empty spaces 
  
  for(int i=0; i < is_in_S_vec.size(); ++i){//pushing only elements not empty values
    if(is_in_S_vec(i)!=""){
      is_in_S_vecf.push_back(is_in_S_vec(i));
    }
    
  }
  
  return(is_in_S_vecf);
}


// Function to check comment element between 2 vectors
// The function returns a vector with the common names
// [[Rcpp::export]]
CharacterVector runit_intersect( CharacterVector x, CharacterVector y){
  return intersect( x, y ) ;
}

// [[Rcpp::export]]
CharacterVector commonelements_VecVec(CharacterVector I_vec,  CharacterVector S_vec){
  
  CharacterVector is_in_S_vecf;// vector containing results of comparison
  CharacterVector dest=clone(I_vec); //copying the vector of interest
  CharacterVector is_in_S_vec; //ceating empty vector to store data
  is_in_S_vec=runit_intersect(S_vec,dest);
  

  for(int i=0; i < is_in_S_vec.size(); ++i){//pushing only elements not empty values
    if(is_in_S_vec(i)!=""){
      is_in_S_vecf.push_back(is_in_S_vec(i));
    }
    
  }
  
  return(is_in_S_vecf);
}


// Given a vector v , and an element in the position d of the vector is_in_Vec 
// finds the prosition of the element in v and return it
// [[Rcpp:export]]
auto FindIndex(CharacterVector v,CharacterVector is_in_Vec, int d){
  String name=is_in_Vec(d);// element we are looking for in v
  int i; //iterator
  int counter=0; // poition of the element searched
  for( i = 0; i < v.size(); ++i) {
    if( v[i] == name ) { // if ith element coincide with eleemnt store the poisition
      counter = i;
      break;
    }
  }
  return counter;
}





// This function estimates for each timestep from (0 to tmax) the nodes that are infected 
// Given an intial vector of infected
// The probability of infection could depend either by the number of infected neighbor (unweighted)
// or by the incoming weight (weighted)

// The function take the list of the destiantion ,check the susceptible ones 
// and for each check how many are already infectd
// Estimate the probability of infection  and infect

// The function returns a list containing 2 eleements:
// curve: a list storing at each time the number of individual in I state
// infection: a list containing for each time the id on individual in I state

// Input
// List Destination: list keys is a destination, and value is the vector of origin connected
// List Weight: list keys are destiantion and values weight of links toward origin . the order is the same as destiantion 
// tmax : duration , number of steps
// CharacterVector S_vec : vectors conating number of nodes susceptible. Is updated every t
// CahracterVector I_vec ; vector containng id nodes infected
// double trans_prob : unit probability per link per time
// bool WEIGHT : if & use the weighted version ,else the unweighted
// SIR Simulator function with comments
// [[Rcpp::export]]
List SIR_simulator(List ListDestination, List ListWeight, int tmax, CharacterVector S_vec,
                   CharacterVector I_vec, double trans_prob, double rec_prob, bool WEIGHT) {
  // Clone the input Lists to avoid modifying the original data
  List Dest = clone(ListDestination);
  List Weight = clone(ListWeight);
  int n = I_vec.size();
  int i, itl, ite, newid,itIvec,itSvec;
  double inf, prob;
  CharacterVector Svec = clone(S_vec);
  CharacterVector Ivec = clone(I_vec);
  CharacterVector Rvec ;  // Initialize an empty vector for recovered individuals
  CharacterVector is_in_Svec;
  CharacterVector newInf0;
  CharacterVector newInf;
  CharacterVector newRec0;
  CharacterVector newRec;
  CharacterVector Infec_neighb;
  NumericVector Infec_neighb_weight0;
  NumericVector Infec_neighb_weight;
  NumericVector etat_t;
  NumericVector infectedindex;
  NumericVector infectedindex0;
  CharacterVector Neighb;
  NumericVector WeightNeighb;
  CharacterVector v;
  string name;
  // Lists for saving results
  List res(tmax + 1);
  List incidence(tmax + 1);
  List epidemic_curve(tmax + 1);
  List results;
  
  
  // Initialization
  incidence[0] = Ivec;
  res[0] = CharacterVector(0);
  etat_t = NumericVector::create(0,Svec.size(), Ivec.size(), Rvec.size(), Ivec.size());
  epidemic_curve[0] = etat_t;
  v = Dest.names();
  
  // Simulation loop over time steps
  for (int tm = 1; tm <= tmax; tm++) {
    if (Ivec.size() > 0) { // check if there are still infectious individuals
      //Rcout << "time " << tm << "Ivec Beginn" <<Ivec.size()<<"\n";
      
      // Find common elements between Dest and Svec
      is_in_Svec = CharacterVector(0);
      is_in_Svec = commonelements_VecVec(v, Svec); //list of possible destiantion that are still Susceptibke
      newInf = CharacterVector(0);
      newRec = CharacterVector(0);
      ////Rcout<<" Enter the loop Susceptible Nodes "<<is_in_Svec.size()<<"\n";
      // Loop over each susceptible node
      for (int d = 0; d < is_in_Svec.size(); d++) {
       // //Rcout<<"Check destination "<<d<<"\n";
        itl = FindIndex(v, is_in_Svec, d);// index susceptible Destination in the Destination List
        
        // Find infected neighbors of the susceptible node
        Neighb=Rcpp::as<Rcpp::CharacterVector>(Dest[itl]);// vector containing list of neighbors of candidate
        
        Infec_neighb=commonelements_VecVec(Ivec,Neighb);//vector containing the list of nfected neighbor
        //Rcout<<"time " << tm <<"Infec neighb "<<Infec_neighb<<"\n";
        WeightNeighb=Rcpp::as<Rcpp::NumericVector>(Weight[itl]);// we find only the infected ones
        WeightNeighb.names()=Neighb;
        
        
        infectedindex = clone(infectedindex0);
        Infec_neighb_weight = clone(Infec_neighb_weight0);
        if(Infec_neighb.size()>0){
          // Find the weights of infected neighbors
          for (int e = 0; e < Infec_neighb.size(); e++) {
            name= Infec_neighb(e);
            
            
            ite = WeightNeighb.findName(name);
            
            infectedindex.push_back(ite);
          }
          
          

          
          
          // Store the weights of infected neighbors
          for (NumericVector::iterator m = infectedindex.begin(); m != infectedindex.end(); ++m) {
            Infec_neighb_weight.push_back(WeightNeighb(*m));
          }
          
          
          // Calculate the infection probability
          if (WEIGHT == 1) {
            double w = sum(Infec_neighb_weight);
            prob = 1 - pow((1 - trans_prob), w);
          } else {
            
            int links = Infec_neighb.size();
            prob = 1 - pow((1 - trans_prob), links);
          }
          
          
          // Generate a random number to decide infection
          inf = R::runif(0.0, 1.0);
          
          
          // Perform infection based on the probability
          if (inf < prob) {//prob
            newInf.push_back(is_in_Svec(d));
            //is_in_Svec.erase(is_in_Svec(d));
            //Svec.erase(is_in_Svec(d));
            ////Rcout << "InfctionSize Svec " << Svec.size() << "\t" << " Size Ivec " << Ivec.size() << "\t" << "Incidence " << newInf.size() << "\n";
          }
        }
        
      }
      ////Rcout<<"Done Infection"<<"\n";
      
      // Generate a random number to decide recovery
      
 
      
      // Recovery process
      for (int i = 0; i < Ivec.size(); i++) { // loop iterates over the elements of the Ivec vector, which contains the IDs of currently infected individuals
        // Generate a random number to decide recovery
        double recoveryRand = R::runif(0.0, 1.0); //  generates a random numbe
        if (recoveryRand< rec_prob) { // Inside the loop, for each infected individual, 
                                              //another random number is generated using R::runif(0.0, 1.0). If this random number is less than the recovery probability (rec_prob), it means the individual recovers.
          newRec.push_back(Ivec(i));            // The ID of the recovered individual is added to the Rvec vector, which keeps track of individuals who have recovered.

        }
      }
      //Rcout<<"Done Recovery"<<"\n";
      if(newInf.size()>0){
       
        for(int r=0;r<newInf.size();r++){

          Ivec.push_back(newInf(r));
          itSvec=FindIndex(Svec,newInf, r);
          Svec.erase(itSvec);
        
        }
        
      }
      // //Rcout<<"Done UpdateI"<<"\n";
      ////Rcout<<"Size newRec"<<newRec.size()<<"\n";
      if(newRec.size()>0){
       for(int l=0;l<newRec.size();++l){
         ////Rcout<<"Index "<<l<<"Rec"<<newRec(l)<<"\n";
         Rvec.push_back(newRec(l));  
         ////Rcout<< "Push"<<Ivec.size()<<"\n";
         itIvec=FindIndex(Ivec,newRec, l);
         Ivec.erase(itIvec);

         
          ////Rcout<< "Erase"<<"\n";
       // The ID of the recovered individual is removed from the Ivec vector, as the individual is no longer infected.
        }
      }
      ////Rcout<<"Done UpdateR"<<"\n";
      // Store the results for the current time step
      res[tm] = newRec;
      incidence[tm] = newInf;
      ////Rcout<<"time "<<tm<<"New Infection"<<newInf<<"\n";
      ////Rcout <<tm << "Size Svec " << Svec.size() << "\t" << " Size Ivec " << Ivec.size() << "\t" << " Size Rvec " << Rvec.size() << "\t" << "Incidence " << newInf.size() << "\n";
      etat_t = NumericVector::create(tm,Svec.size(), Ivec.size(), Rvec.size(),newInf.size());
      epidemic_curve[tm] = etat_t;
      newInf=CharacterVector(0);
      newRec=CharacterVector(0);
      is_in_Svec=CharacterVector(0);

    } else {
      
      break;
    }
  }
  
  
  // Eliminate null elements
  LogicalVector to_keep(tmax + 1);
  for (int i = 0; i < tmax; i++) {
    to_keep[i] = !Rf_isNull(epidemic_curve[i]);
  }
  
  
  // Store and return the results
  results["Incidence"] = incidence[to_keep];
  results["Recovery"] = res[to_keep];
  results["curve"] = epidemic_curve[to_keep];
  return (results);
}
  
  