#include <Rcpp.h>
using namespace Rcpp;
using namespace std;


// Function to check comment element between Column of a datframe and a vector
// The function compares the name stored in a column (index given column_number) of a dataframe (net)
// and those in a vector (S_vec)
// The function returns a vector with the cmmon names
// [[Rcpp::export]]
  CharacterVector commonelements_DfVec(DataFrame net,  CharacterVector S_vec, int column_number){
    DataFrame filtered_net = clone(net);
    CharacterVector is_in_S_vecf; // vector containing results of comaprison
    CharacterVector dest=filtered_net[column_number]; // selecting column of interest
    CharacterVector is_in_S_vec(S_vec.size() + dest.size()); //creating empty vector containing common elements
    
    //iterator to store return type
    CharacterVector::iterator it, end;
    
    end = set_intersection(
      S_vec.begin(), S_vec.end(),
      dest.begin(), dest.end(),
      is_in_S_vec.begin());// vector containing common elements and empty spaces 
    
    for(int i=0; i < is_in_S_vec.size(); ++i){//pushing only elements not empty values
      if(is_in_S_vec(i)!=""){
        is_in_S_vecf.push_back(is_in_S_vec(i));
      }
      
    }
    
    return(is_in_S_vecf);
  }
  // [[Rcpp::export]]
  CharacterVector commonelements_ListVec(List net,  CharacterVector S_vec){
    List filtered_net = net;
    CharacterVector is_in_S_vecf;// vector containing results of comaprison
    CharacterVector dest=filtered_net.names();// extracting the names of the list key
    CharacterVector is_in_S_vec(S_vec.size() + dest.size());//creating empty vector containing common elements
    
    //iterator to store return type
    CharacterVector::iterator it, end;
    
    end = set_intersection(
      S_vec.begin(), S_vec.end(),
      dest.begin(), dest.end(),
      is_in_S_vec.begin());// vector containing common elements and empty spaces 
    
    for(int i=0; i < is_in_S_vec.size(); ++i){//pushing only elements not empty values
      if(is_in_S_vec(i)!=""){
        is_in_S_vecf.push_back(is_in_S_vec(i));
      }
      
    }
    
    return(is_in_S_vecf);
  }
  
  
  // Function to check comment element between 2 vectors
  // The function returns a vector with the common names
  // [[Rcpp::export]]
  CharacterVector runit_intersect( CharacterVector x, CharacterVector y){
    return intersect( x, y ) ;
  }
  
  // [[Rcpp::export]]
  CharacterVector commonelements_VecVec(CharacterVector I_vec,  CharacterVector S_vec){
    
    CharacterVector is_in_S_vecf;// vector containing results of comparison
    CharacterVector dest=clone(I_vec); //copying the vector of interest
    CharacterVector is_in_S_vec; //ceating empty vector to store data
    is_in_S_vec=runit_intersect(S_vec,dest);
    
    cout<<"Functioncommelements vecvec"<<"\t"<<is_in_S_vec<<"\n";
    cout<<"dest"<<dest<<"\t"<<"S_vec"<<S_vec<<"\n";
    for(int i=0; i < is_in_S_vec.size(); ++i){//pushing only elements not empty values
      if(is_in_S_vec(i)!=""){
        is_in_S_vecf.push_back(is_in_S_vec(i));
      }
      
    }
    
    return(is_in_S_vecf);
  }
  
  
  // Given a vector v , and an element in the position d of the vector is_in_Vec 
  // finds the prosition of the element in v and return it
  // [[Rcpp:export]]
  auto FindIndex(CharacterVector v,CharacterVector is_in_Vec, int d){
    String name=is_in_Vec(d);// element we are looking for in v
    int i; //iterator
    int counter=0; // poition of the element searched
    for( i = 0; i < v.size(); ++i) {
      if( v[i] == name ) { // if ith element coincide with eleemnt store the poisition
        counter = i;
        break;
      }
    }
    return counter;
  }
  

// This function estimates for each timestep from (0 to tmax) the nodes that are infected 
// Given an intial vector of infected
// The probability of infection could depend either by the number of infected neighbor (unweighted)
// or by the incoming weight (weighted)

// The function take the list of the destiantion ,check the susceptible ones 
// and for each check how many are already infectd
// Estimate the probability of infection  and infect

// The function returns a list containing 2 eleements:
// curve: a list storing at each time the number of individual in I state
// infection: a list containing for each time the id on individual in I state

// Input
// List Destination: list keys is a destination, and value is the vector of origin connected
// List Weight: list keys are destiantion and values weight of links toward origin . the order is the same as destiantion 
// tmax : duration , number of steps
// CharacterVector S_vec : vectors conating number of nodes susceptible. Is updated every t
// CahracterVector I_vec ; vector containng id nodes infected
// double trans_prob : unit probability per link per time
// bool WEIGHT : if & use the weighted version ,else the unweighted
// SIR Simulator function with comments
// [[Rcpp::export]]
List SIR_simulator(List ListDestination, List ListWeight, int tmax, CharacterVector S_vec,
                   CharacterVector I_vec, CharacterVector R_vec, double trans_prob, double rec_prob, bool WEIGHT) {
  // Clone the input Lists to avoid modifying the original data
  List Dest = clone(ListDestination);
  List Weight = clone(ListWeight);
  int n = I_vec.size();
  int i, itl, ite, newid;
  double inf, prob;
  CharacterVector Svec = clone(S_vec);
  CharacterVector Ivec = clone(I_vec);
  CharacterVector Rvec = clone(R_vec);
  CharacterVector is_in_Svec;
  CharacterVector newInf0;
  CharacterVector newInf;
  CharacterVector Infec_neighb;
  NumericVector Infec_neighb_weight0;
  NumericVector Infec_neighb_weight;
  NumericVector etat_t;
  NumericVector infectedindex;
  NumericVector infectedindex0;
  CharacterVector Neighb;
  NumericVector WeightNeighb;
  CharacterVector v;
  
  // Lists for saving results
  List res(tmax + 1);
  List incidence(tmax + 1);
  List epidemic_curve(tmax + 1);
  List results;
  
  // Initialization
  incidence[0] = I_vec;
  res[0] = I_vec;
  etat_t = NumericVector::create(Svec.size(), Ivec.size(), 0);
  epidemic_curve[0] = etat_t;
  
  // Simulation loop over time steps
  for (int tm = 1; tm <= tmax; tm++) {
    if (Svec.size() > 0) {
      cout << "time " << tm << "\n";
      v = Dest.names();
      
      // Find common elements between Dest and Svec
      is_in_Svec = commonelements_ListVec(Dest, Svec);
      newInf = clone(newInf0);
      
      // Loop over each susceptible node
      for (int d = 0; d < is_in_Svec.size(); ++d) {
        itl = FindIndex(v, is_in_Svec, d);
        Neighb = Rcpp::as<Rcpp::CharacterVector>(Dest[itl]);
        
        // Find infected neighbors of the susceptible node
        Infec_neighb = commonelements_ListVec(Dest, Svec);
        WeightNeighb = Rcpp::as<Rcpp::NumericVector>(Weight[itl]);
        WeightNeighb.names() = Neighb;
        
        infectedindex = clone(infectedindex0);
        
        // Find the weights of infected neighbors
        for (int e = 0; e < Infec_neighb.size(); ++e) {
          String name = Infec_neighb(e);
          ite = WeightNeighb.findName(name);
          infectedindex.push_back(ite);
        }
        
        
        Infec_neighb_weight = clone(Infec_neighb_weight0);
        
        // Store the weights of infected neighbors
        for (NumericVector::iterator m = infectedindex.begin(); m != infectedindex.end(); ++m) {
          Infec_neighb_weight.push_back(WeightNeighb(*m));
        }
        
        
        // Calculate the infection probability
        if (WEIGHT == 1) {
          int w = sum(Infec_neighb_weight);
          prob = 1 - pow((1 - trans_prob), w);
        } else {
          
          int l = Infec_neighb.size();
          prob = 1 - pow((1 - trans_prob), l);
        }
        
        
        // Generate a random number to decide infection
        inf = R::runif(0.0, 1.0);
        
        // Perform infection based on the probability
        if (inf < prob) {
          newInf.push_back(is_in_Svec(d));
          Ivec.push_back(is_in_Svec(d));
          Svec.erase(is_in_Svec(d));
          is_in_Svec.erase(is_in_Svec(d));
          cout << "Size Svec " << Svec.size() << " Size Ivec " << Ivec.size() << "\t" << "Incidence " << newInf.size() << "\n";
        }
      }
      
      // Generate a random number to decide recovery
      double recoveryRand = R::runif(0.0, 1.0);
      
    
      
      // Recovery process
      CharacterVector recovered;
      for (int i = 0; i < Ivec.size(); ++i) {
        // Generate a random number to decide recovery
        if (R::runif(0.0, 1.0) < rec_prob) { //  Pour chaque nœud,  génèreR un nombre aléatoire entre 0 et 1 PUIS ON COMPAR A LA PROB DE RECUP
          recovered.push_back(Ivec(i)); //  Si le nombre aléatoire est inférieur à la probabilité de récupération, le nœud récupère, et ses identifiants sont déplacés vers le vecteur recovered, retirés du vecteur Ivec, et ajoutés au vecteur Svec.
          Svec.push_back(Ivec(i)); 
          Ivec.erase(Ivec(i)); //  permet de supprimer l'élément actuel tout en maintenant la cohérence de l'itération
        }
      }
      
      // Perform recovery based on the probability
      if (recoveryRand < rec_prob) {
        recovered.push_back(Ivec(i));
        Svec.push_back(Ivec(i));
        Ivec.erase(Ivec(i));
      }
      
      
      // Store the results for the current time step
      res[tm] = Ivec;
      incidence[tm] = newInf;
      etat_t = NumericVector::create(Svec.size(), Ivec.size(), recovered.size());
      epidemic_curve[tm] = etat_t;
    } else {
      
      break;
    }
  }
  
  
  // Eliminate null elements
  LogicalVector to_keep(tmax + 1);
  for (int i = 0; i < tmax; i++) {
    to_keep[i] = !Rf_isNull(epidemic_curve[i]);
  }
  
  
  // Store and return the results
  results["Incidence"] = incidence[to_keep];
  results["Infection"] = res[to_keep];
  results["Recovery"] = epidemic_curve[to_keep];
  return (results);
}

