---
title: "Script pour choisir Nsim"
author: "Asma MESDOUR"
date: "2024-03-08"
output: html_document
---

Variation du nombre de simulation pour un choix optimal...

#library 

```{r message=FALSE, warning=FALSE, include=FALSE}
library(ggplot2)
library(dplyr)
library(network)
library(igraph)
<<<<<<< HEAD
library(bc3net)
library(tidyverse)
library(GGally)
library(questionr)
library(broom)
library(broom.helpers)
library(gtsummary)
library(factoextra)
library(FactoMineR)
library(glmnet)
#library(graph4lg)
library(plotmo)
library(cluster)
library(rpart)
library(rpart.plot)
library(devtools)
library(OneR)
library(rfviz)
library(tidyverse)
library(glmnet)
library(plotly)
library(reticulate)
library(cluster)
library(fpc)
library(aricode)
library(VertexSort)
library(cowplot)
library(gplots)
library(cluster)
library(flexclust)
#library(fpc)
library(mclust)
library(ggpubr)
library(sf)
#library(ComplexUpset)
library(UpSetR)
=======
>>>>>>> 6c108a8d3d5d96e0f165950d6232d2d4f42f8acd
library(Rcpp)
library(purrr)
```

1. Network 


```{r include=FALSE}
Link <- read_delim("../1_Data/OfficialMobilityData.csv", 
    delim = ";", escape_double = FALSE, trim_ws = TRUE) # movements
```

```{r include=FALSE}
Loc <- read_delim("../1_Data/OfficialLocationData.csv", 
    delim = ";", escape_double = FALSE, trim_ws = TRUE) # locations
```

```{r echo=FALSE}
PlatNet<-subset(Link,ORIGIN!=DESTINATION)
net<-PlatNet
net<-aggregate(HERDSIZE~ORIGIN+DESTINATION,net,sum)
networkNGR<-network(net, directed = TRUE)
```


2. Optimisation du nombre d'iteration


```{r}
sourceCpp("provascriptSIR_with_probRecov.cpp")
```

```{r}
# Selection de I_vec (indegree superieur à 0)
graph <- graph_from_data_frame(net, directed = TRUE)
<<<<<<< HEAD
indegree <- degree(graph, mode = "in")
noeuds_indegree_superieur_a_0 <- names(indegree[indegree > 0])
outdegree <- degree(graph, mode = "out")
noeuds_outdegree_superieur_a_0 <- names(indegree[outdegree > 0])
=======
outdegree <- degree(graph, mode = "out")
noeuds_outdegree_superieur_a_0 <- names(outdegree[outdegree > 0])
>>>>>>> 6c108a8d3d5d96e0f165950d6232d2d4f42f8acd
```



```{r}
# parametres
unique_nodes <- unique(Loc$NODE_ID) #S
S_vec_all<- as.vector(unique_nodes)
WEIGHT<-TRUE
tmax = 30
trans_prob = 0.1
rec_prob = 0.1
ListWeight <- split(net$HERDSIZE, net$DESTINATION)
ListDestination<- split(net$ORIGIN, net$DESTINATION)

```

        

```{r}
#variation de Nsim = 50
#modele SIR 
<<<<<<< HEAD
sourceCpp("provascriptSIR_with_probRecov.cpp")
results_100 <- list()
SIR_100 <- for (i in 1:1) {
  I_vec <- sample(noeuds_outdegree_superieur_a_0,1)
  S_vec <- setdiff(noeuds_indegree_superieur_a_0,I_vec) 
  # Utilisez Random_node dans votre simulation SIR_simulator
  results_100[[i]] <- SIR_simulator(ListDestination, ListWeight, tmax=3, S_vec, I_vec, rec_prob = 0.01, trans_prob = 1.0, WEIGHT = TRUE)
}
results_100[[1]]$curve
saveRDS(results_50, file = "res_IvecRandom_SI_3.rds")
=======
results_50 <- list()
SIR_50 <- for (i in 1:50) {
  I_vec <- sample(noeuds_outdegree_superieur_a_0, 1)
  
  # Utilisez Random_node dans votre simulation SIR_simulator
  results_50[[i]] <- SIR_simulator(ListDestination, ListWeight, tmax = tmax, S_vec, I_vec, rec_prob = rec_prob, trans_prob = trans_prob, WEIGHT = TRUE)
}
saveRDS(results_50, file = "res_IvecRandom_SIR_50.rds")
>>>>>>> 6c108a8d3d5d96e0f165950d6232d2d4f42f8acd
#variation de Nsim = 100
#modele SIR 
results_100 <- list()
SIR_100 <- for (i in 1:100) {
  I_vec <- sample(noeuds_indegree_superieur_a_0, 1)
 results_100[[i]] <- SIR_simulator(ListDestination, ListWeight, tmax, S_vec, I_vec, rec_prob = recov_prob, trans_prob = trans_prob, WEIGHT = TRUE)
}
saveRDS(results_100, file = "res_IvecRandom_SIR_100.rds")

#variation de Nsim = 250
#modele SIR 
results_250 <- list()
SIR_250 <- for (i in 1:250) {
 I_vec <- sample(noeuds_indegree_superieur_a_0, 1)
  
  results_250[[i]] <- SIR_simulator(ListDestination, ListWeight, tmax, S_vec, I_vec, rec_prob = recov_prob, trans_prob = trans_prob, WEIGHT = TRUE)
}
saveRDS(results_250, file = "res_IvecRandom_SIR_250.rds")

#variation de Nsim = 500
#modele SIR 
results_500 <- list()
SIR_500 <- for (i in 1:500) {
 I_vec <- sample(noeuds_indegree_superieur_a_0, 1)
  
  results_500[[i]] <- SIR_simulator(ListDestination, ListWeight, tmax, S_vec, I_vec, rec_prob = recov_prob, trans_prob = trans_prob, WEIGHT = TRUE)
}
saveRDS(results_500, file = "res_IvecRandom_SIR_500.rds")

#variation de Nsim = 750
#modele SIR 
results_750 <- list()
SIR_750 <- for (i in 1:750) {
 I_vec <- sample(noeuds_indegree_superieur_a_0, 1)
  
  results_750[[i]] <- SIR_simulator(ListDestination, ListWeight, tmax, S_vec, I_vec, rec_prob = recov_prob, trans_prob = trans_prob, WEIGHT = TRUE)
}
saveRDS(results_750, file = "res_IvecRandom_SIR_750.rds")

#variation de Nsim = 250
#modele SIR 
results_1000 <- list()
SIR_1000 <- for (i in 1:1000) {
 I_vec <- sample(noeuds_indegree_superieur_a_0, 1)
  
  results_1000[[i]] <- SIR_simulator(ListDestination, ListWeight, tmax, S_vec, I_vec, rec_prob = recov_prob, trans_prob = trans_prob, WEIGHT = TRUE)
}
saveRDS(results_1000, file = "res_IvecRandom_SIR_1000.rds")
```


3. Description des Outputs : taille finale / IC 

```{r}
#clean output sim = 50
df_50 <- map_df(results_50, ~ {
  iteration <- .x$curve
  data.frame(
    iteration = rep(seq_along(results_50), each = length(iteration)),
    time = rep(seq_along(iteration), times = length(results_50)),
    S = sapply(iteration, "[[", 1),
    I = sapply(iteration, "[[", 2),
    R = sapply(iteration, "[[", 3)
  )
})

# en attendant la solution c ça
distinct_50 <- df_50 %>%
  group_by(iteration, time) %>%
  slice(1) %>%
  ungroup()

#clean output sim = 100
df_100 <- map_df(results_100, ~ {
  iteration <- .x$curve
  data.frame(
    iteration = rep(seq_along(results_100), each = length(iteration)),
    time = rep(seq_along(iteration), times = length(results_100)),
    S = sapply(iteration, "[[", 1),
    I = sapply(iteration, "[[", 2),
    R = sapply(iteration, "[[", 3)
  )
})

# en attendant la solution c ça
distinct_100 <- df_100 %>%
  group_by(iteration, time) %>%
  slice(1) %>%
  ungroup()

#clean output sim = 500
df_500 <- map_df(results, ~ {
  iteration <- .x$curve
  data.frame(
    iteration = rep(seq_along(results), each = length(iteration)),
    time = rep(seq_along(iteration), times = length(results)),
    S = sapply(iteration, "[[", 1),
    I = sapply(iteration, "[[", 2),
    R = sapply(iteration, "[[", 3)
  )
})

# en attendant la solution c ça
distinct_500 <- df_500 %>%
  group_by(iteration, time) %>%
  slice(1) %>%
  ungroup()
```

#description des courbes SIR 

```{r}
psim<-ggplot(data=distinct_50)+geom_line(aes(x=time, y=I,group = iteration),col="red",alpha=0.3)
psim<-psim+geom_line(aes(x=time, y=S,group = iteration),col="green",alpha=0.3)
psim<-psim+geom_line(aes(x=time,y= R,group = iteration),col="blue",alpha=0.3)
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, I),col="red",geom = "smooth") 
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, S),col="green",geom = "smooth") 
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, R),col="blue",geom = "smooth") 
psim<-psim+ theme_bw()+labs(y="Number")
psim
```

```{r}
psim<-ggplot(data=distinct_100)+geom_line(aes(x=time, y=I,group = iteration),col="red",alpha=0.3)
psim<-psim+geom_line(aes(x=time, y=S,group = iteration),col="green",alpha=0.3)
psim<-psim+geom_line(aes(x=time,y= R,group = iteration),col="blue",alpha=0.3)
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, I),col="red",geom = "smooth") 
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, S),col="green",geom = "smooth") 
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, R),col="blue",geom = "smooth") 
psim<-psim+ theme_bw()+labs(y="Number")
psim
```
```{r}
psim<-ggplot(data=distinct_500)+geom_line(aes(x=time, y=I,group = iteration),col="red",alpha=0.3)
psim<-psim+geom_line(aes(x=time, y=S,group = iteration),col="green",alpha=0.3)
psim<-psim+geom_line(aes(x=time,y= R,group = iteration),col="blue",alpha=0.3)
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, I),col="red",geom = "smooth") 
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, S),col="green",geom = "smooth") 
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, R),col="blue",geom = "smooth") 
psim<-psim+ theme_bw()+labs(y="Number")
psim
```
les seed tiré au hasad sont forcément ceux avec un indegree = 1, peut etre que prob_recov = 0.1 est trés elevé ? 

```{r}
# Utilisez la fonction dplyr pour agréger et sélectionner le maximum de la colonne "I" par itération
data_for_plot <- distinct_50 %>%
  group_by(iteration) %>%
  summarise(taille_finale = max(I))

ggplot(data_for_plot, aes(x = iteration, y = taille_finale)) +
  geom_point() +  # Points pour la taille finale de chaque itération
  geom_line(aes(x = iteration, y = mean(taille_finale)), color = "red", linetype = "dashed", size = 1) +  # Ligne pour la moyenne
  labs(title = "Taille finale de l'épidémie en fonction des itérations",
       x = "Itération",
       y = "Taille finale de l'épidémie") +
  theme_minimal()
```

```{r}
library(dplyr)

# Utilisez la fonction dplyr pour agréger et calculer la moyenne de la colonne "I" par itération
data_50 <- distinct_50 %>%
  summarise(taille_finale_moyenne = mean(I))

data_100 <- distinct_100 %>%
  summarise(taille_finale_moyenne = mean(I))

data_500 <- distinct_500 %>%
  summarise(taille_finale_moyenne = mean(I))

combined_data <- bind_rows(
  mutate(data_50, dataset = "50"),
  mutate(data_100, dataset = "100"),
  mutate(data_500, dataset = "500")
)
library(DescTools)
# Agréger pour obtenir la moyenne globale par dataset
global_means <- combined_data %>%
  group_by(dataset) %>%
  summarise(taille_finale_moyenne = mean(taille_finale_moyenne),
            taille_finale_sd = if (length(unique(taille_finale_moyenne)) > 1) sd(taille_finale_moyenne) else 0,
            ci_lower = if (length(unique(taille_finale_moyenne)) > 1) taille_finale_moyenne - 1.96 * taille_finale_sd / sqrt(n()) else taille_finale_moyenne,
            ci_upper = if (length(unique(taille_finale_moyenne)) > 1) taille_finale_moyenne + 1.96 * taille_finale_sd / sqrt(n()) else taille_finale_moyenne)

# Créer un graphique avec histogramme et intervalles de confiance
ggplot(global_means, aes(x = dataset, y = taille_finale_moyenne, fill = dataset)) +
  geom_col(position = "dodge", width = 0.7) +
  geom_errorbar(aes(ymin = ci_lower, ymax = ci_upper), position = position_dodge(width = 0.7), width = 0.25) +
  labs(title = "Moyenne globale de la taille finale de l'épidémie avec intervalles de confiance",
       x = "Dataset",
       y = "Moyenne globale de la taille finale de l'épidémie") +
  theme_minimal()

```




```{r}
psim<-ggplot(data=distinct_500)+geom_line(aes(x=time, y=I,group = iteration),col="red",alpha=0.3)
psim<-psim+geom_line(aes(x=time, y=S,group = iteration),col="green",alpha=0.3)
psim<-psim+geom_line(aes(x=time,y= R,group = iteration),col="blue",alpha=0.3)
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, I),col="red",geom = "smooth") 
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, S),col="green",geom = "smooth") 
psim<-psim+ stat_summary(fun.data ="mean_sdl",aes(x=time, R),col="blue",geom = "smooth") 
psim<-psim+ theme_bw()+labs(y="Number")
psim
```

```{r}
library(dplyr)

# Utilisez la fonction dplyr pour agréger et calculer la moyenne de la colonne "I" par itération
data_50 <- distinct_50 %>%
  summarise(taille_finale_moyenne = mean(I))

data_100 <- distinct_100 %>%
  summarise(taille_finale_moyenne = mean(I))

data_500 <- distinct_500 %>%
  summarise(taille_finale_moyenne = mean(I))

combined_data <- bind_rows(
  mutate(data_50, dataset = "50"),
  mutate(data_100, dataset = "100"),
  mutate(data_500, dataset = "500")
)
library(DescTools)
# Agréger pour obtenir la moyenne globale par dataset
global_means <- combined_data %>%
  group_by(dataset) %>%
  summarise(taille_finale_moyenne = mean(taille_finale_moyenne),
            taille_finale_sd = if (length(unique(taille_finale_moyenne)) > 1) sd(taille_finale_moyenne) else 0,
            ci_lower = if (length(unique(taille_finale_moyenne)) > 1) taille_finale_moyenne - 1.96 * taille_finale_sd / sqrt(n()) else taille_finale_moyenne,
            ci_upper = if (length(unique(taille_finale_moyenne)) > 1) taille_finale_moyenne + 1.96 * taille_finale_sd / sqrt(n()) else taille_finale_moyenne)

# Créer un graphique avec histogramme et intervalles de confiance
ggplot(global_means, aes(x = dataset, y = taille_finale_moyenne, fill = dataset)) +
  geom_col(position = "dodge", width = 0.7) +
  geom_errorbar(aes(ymin = ci_lower, ymax = ci_upper), position = position_dodge(width = 0.7), width = 0.25) +
  labs(title = "Moyenne globale de la taille finale de l'épidémie avec intervalles de confiance",
       x = "Dataset",
       y = "Moyenne globale de la taille finale de l'épidémie") +
  theme_minimal()

```
